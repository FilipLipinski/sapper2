﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper
{
    class Delegates
    {
        public delegate int Delegate1(int arg1, int arg2);

        public delegate int Delegate2(int arg);

        public int Add(int arg1, int arg2)
        {
            return arg1 + arg2;
        }

        public int Subtract(int arg1, int arg2)
        {
            return arg1 - arg2;
        }
    }


    class Linq
    {
        private string[] fruitsStrings;
        private List<Car> carsList;
        private IEnumerable<string> linqFruitsEnumerable;
        private IEnumerable<Car> linqCarsEnumerable;
        public Linq()
        {
            string[] fruits = { "Apple", "Pear", "Banana", "Orange", "Watermelon", "Grapes", "Pineapple", "Plum", "Cherry", "Strawberry", "Raspberry" };
            carsList = new List<Car>{new Car{Id = 1, Brand = "BMW", Model = "E37"},
                new Car{Id = 2, Brand = "Fiat", Model = "Tipo"},
                new Car{Id = 3, Brand = "Skoda", Model = "Octavia"}};
            fruitsStrings = fruits;
        }

        public void Select()
        {
            var list = from f in fruitsStrings select f;
            linqFruitsEnumerable = list;
        }
        public void SelectWhere()
        {
            var list = from f in fruitsStrings
                       where f.StartsWith("P") 
                       select f;
            linqFruitsEnumerable = list;
        }
        public void SelectWhere2()
        {
            var list = from f in fruitsStrings
                where f.ToLower().Contains("apple")
                select f;
            linqFruitsEnumerable = list;
        }
        public void OrderAlphabetically()
        {
            var list = from f in fruitsStrings
                orderby f
                select f;
            linqFruitsEnumerable = list;
        }
        public void SortCarsByModelName()
        {
            var list = from c in carsList
                orderby c.Model
                select c;
            linqCarsEnumerable = list;
        }
        public void SelectSingleCarById()
        {
            var singleCar = from c in carsList
                where c.Id == 1
                select c;
            linqCarsEnumerable = singleCar;
        }

        public void SelectSingleCarInstance()
        {
            var singleCar = (from c in carsList
                where c.Id.Equals(3)
                select c).Single();
            Console.WriteLine("Brand: " + singleCar.Brand + "\nModel: " + singleCar.Model);
        }

        public void SelectRangeUsingLambda()
        {
            var list = carsList.Where(c => c.Id > 1);
            linqCarsEnumerable = list;
        }


        public void PrintFruits()
        {
            foreach (var fruit in linqFruitsEnumerable)
            {
                Console.WriteLine(fruit);
            }
        }

        public void PrintCars()
        {
            foreach (var car in linqCarsEnumerable)
            {
                Console.WriteLine(car.Brand + "" + car.Model);
            }
        }
    }

    class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
    }
}
