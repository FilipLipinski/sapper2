﻿using System;
using Minesweeper.BoardClasses;

namespace Minesweeper
{
    class Program
    {
        static void Main(string[] args)
        {
            Game newGame = new Game();

            #region InterfacesRegion
            /*
            IFlyable flyable = new FlyingCreature();
            flyable.Fly(1);
            //flyable.Walk(1); Walk is not possible as IFlyable is not implementing this method
            FlyingCreature flyable2 = new FlyingCreature();
            flyable2.Fly(1);
            flyable2.Walk(1);
            flyable2.Eat(MealSize.Small);
            flyable2.TakeABreath(1);



            IWalkable walkable = new WalkingCreature();
            IWalkable walkable2 = new FlyingCreature();
            WalkingCreature walkable3 = new WalkingCreature();
            walkable.Walk(1);
            //walkable.Run(1); Run is not possible as IWalkable is not implementing this method
            walkable2.Walk(1);
            //walkable2.Fly(1); Fly is not possible as IWalkable is not implementing this method even if using FlyingCreature class instance
            walkable3.Walk(1);
            walkable3.Run(1);
            walkable3.Eat(MealSize.Large);
            walkable3.TakeABreath(1);
            */
            #endregion

            #region DelagatesRegion
            /*
            Delegates d = new Delegates();
            Delegates.Delegate1 add = d.Add;
            int addResult = add(2, 2);

            // anonymous function
            Delegates.Delegate1 substract = delegate (int arg1, int arg2) { return arg1 - arg2; };
            int substractionResults = substract(3, 1);

            // initialize using lambda
            Delegates.Delegate1 substract2 = (arg1, arg2) => arg1 - arg2;
            substractionResults = substract2(5, 2);

            Delegates.Delegate2 square = i => i * i;
            Delegates.Delegate2 someCalculations = (i) =>
            {
                int constant = 3;
                return i * i + constant;
            };

            Func<string> text = () => "text" + "text2";
            


            //Linq section
            var linq = new Linq();
            linq.Select();
            linq.PrintFruits();

            linq.SelectWhere();
            linq.PrintFruits();

            linq.SelectWhere2();
            linq.PrintFruits();

            linq.OrderAlphabetically();
            linq.PrintFruits();

            linq.SortCarsByModelName();
            linq.PrintCars();

            linq.SelectSingleCarInstance();

            linq.SelectRangeUsingLambda();
            linq.PrintCars();
            */
            #endregion
        }
    }
}
