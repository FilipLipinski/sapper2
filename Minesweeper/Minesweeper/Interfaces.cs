﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper
{
    interface ICreature
    {
        void TakeABreath(int breathDuration);
        void Eat(MealSize mealSize);
    }

    interface IWalkable
    { void Walk(int walkDuration); }

    interface IFlyable
    { void Fly(int flightDuration); }

    enum MealSize
    {
        Small,
        Medium,
        Large
    }

    abstract class SurfaceCreature : ICreature
    {
        protected int Energy { get; set; }
        protected int HungerLevel { get; set; }
        protected int BloodOxygenLevel { get; set; }

        private int Breathe(int breathDuration)
        {
            if (0 < breathDuration && breathDuration < 2) return -1;
             return -2;
        }

        public void TakeABreath(int breathDuration)
        {
            Energy -= Breathe(breathDuration);
            BloodOxygenLevel++;
        }

        public void Eat(MealSize mealSize)
        {
            switch (mealSize)
            {
                case MealSize.Small:
                    HungerLevel -= 1;
                    break;
                case MealSize.Medium:
                    HungerLevel -= 2;
                    break;
                case MealSize.Large:
                    HungerLevel -= 3;
                    break;
            }
        }
    }

    class WalkingCreature : SurfaceCreature, IWalkable
    {
        public void Walk(int walkDuration)
        {
            throw new NotImplementedException();
        }

        public void Run(int runDuration)
        {
            throw new NotImplementedException();
        }
    }

    class FlyingCreature: SurfaceCreature, IWalkable, IFlyable
    {
        public void Walk(int walkDuration)
        {
            throw new NotImplementedException();
        }

        public void Fly(int flightDuration)
        {
            throw new NotImplementedException();
        }
    }


}
