﻿using System;
using System.Collections.Generic;
using System.Net.Mime;

namespace Minesweeper.BoardClasses
{
    public class Game
    {
        private Board GameBoard { get; set; }

        public Game()
        {
            StartGame();
            bool bombRevealed = false;
            while (!bombRevealed)
            {
                if (GameFinished())
                {
                    Console.WriteLine("Congratulations, all bombs marked!");
                    Console.ReadLine();
                    break;
                }

                bombRevealed = GetUserAction();
            }
        }

        private void StartGame()
        {
            Console.WriteLine("Minesweeper game - Find all mines before they explode!");
            Console.WriteLine("Do you want to use default board size? (y/n)");
            string useDefaultBoardSize = Console.ReadLine();
            if (useDefaultBoardSize != null && useDefaultBoardSize.ToLower() == "y")
            {
                GameBoard = new Board();
            }
            else if (useDefaultBoardSize != null && useDefaultBoardSize.ToLower() == "n")
            {
                //handle custom board size
            }
        }

        private bool GetUserAction()
        {
            string userAction = "";
            bool bombRevealed = false;
            while (userAction != "r" || userAction != "m")
            {
                Console.Clear();
                Console.WriteLine("Minesweeper game - Find all mines before they explode!");
                GameBoard.PrintBoard();
                Console.WriteLine("Do you want to (r)eveal or (m)ark a field?");
                userAction = Console.ReadLine();
                if (userAction != null && userAction.ToLower() == "r")
                {
                    Tuple<int?, int?> fieldLocation = GetFieldLocation();
                    GameBoard.RevealField((int) fieldLocation.Item1 - 1, (int) fieldLocation.Item2 - 1);
                    if (GameBoard.Fields[(int) fieldLocation.Item1 - 1, (int) fieldLocation.Item2 - 1].Reveal())
                    {
                        Console.WriteLine("Oops! <bomb explosion>");
                        Console.ReadLine();
                        bombRevealed = true;
                        break;
                    }
                }
                else if (userAction != null && userAction.ToLower() == "m")
                {
                    Tuple<int?, int?> fieldLocation = GetFieldLocation();
                    GameBoard.Fields[(int)fieldLocation.Item1 -1, (int)fieldLocation.Item2 - 1].Mark();
                }
                else
                {
                    Console.WriteLine("Command unrecognized...");
                }
                Console.ReadLine();
            }
            return bombRevealed;
        }

        private Tuple<int?, int?> GetFieldLocation()
        {
            int? row = null;
            int? column = null;
            while (row == null || row < 1 || row > GameBoard.NumberOfColumns)
            {
                Console.WriteLine("Specify row of field to reveal:");
                row = ToNullableInt(Console.ReadLine());
                if(row == null || row < 1 || row > GameBoard.NumberOfColumns)
                    Console.WriteLine("Incorrect value...");
            }
            while (column == null || column < 1 || column > GameBoard.NumberOfRows)
            {
                Console.WriteLine("Specify column of field to reveal:");
                column = ToNullableInt(Console.ReadLine());
                if(column == null || column < 1 || column > GameBoard.NumberOfRows)
                    Console.WriteLine("Incorrect value...");
            }
            return new Tuple<int?, int?>(row, column);
        }

        private bool GameFinished()
        {
            var listOfFieldsWithBombs = new List<BoardField>();
            var listOfMarkedFields = new List<BoardField>();

            foreach (var field in GameBoard.Fields)
            {
                if(field.HasMine)
                    listOfFieldsWithBombs.Add(field);
            }

            foreach (var field in GameBoard.Fields)
            {
                if (field.IsMarked)
                    listOfMarkedFields.Add(field);
            }

            foreach (var fieldWithBomb in listOfFieldsWithBombs)
            {
                if (!listOfMarkedFields.Contains(fieldWithBomb))
                    return false;
            }

            return true;
        }

        private int? ToNullableInt(string s)
        {
            if (int.TryParse(s, out var i)) return i;
            return null;
        }
    }
}
