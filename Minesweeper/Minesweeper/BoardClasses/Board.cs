﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper.BoardClasses
{
    public class Board
    {
        public int NumberOfRows { get; }
        public int NumberOfColumns { get; }
        public BoardField [,] Fields { get; set; }
        public int NumberOfMines { get; set; }

        public Board(int numberOfRows = 8, int numberOfColumns = 8, int numberOfMines = 12)
        {
            NumberOfRows = numberOfRows;
            NumberOfColumns = numberOfColumns;
            NumberOfMines = numberOfMines;

            Fields = PopulateBoard(numberOfRows, numberOfColumns, numberOfMines);
            SetFieldsValues();
        }

        public void PrintBoardMines()
        {
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    Console.Write(Fields[row, column].HasMine ? "1 " : "0 ");
                }
                Console.Write("\n");
            }
        }

        public void PrintBoardValues()
        {
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    Console.Write(Fields[row, column].FieldValue + " ");
                }
                Console.Write("\n");
            }
        }

        public void PrintBoard()
        {
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    if (Fields[row, column].HasMine && Fields[row, column].IsRevealed) Console.Write("B ");
                    else if (Fields[row, column].IsRevealed) Console.Write(Fields[row, column].FieldValue + " ");
                    else if (Fields[row, column].IsMarked) Console.Write(Fields[row, column].FieldValue + "M ");
                    else Console.Write("* ");
                }
                Console.Write("\n");
            }
        }

        public bool RevealField(int row, int column)
        {
            bool isMineRevealed = Fields[row, column].Reveal();

            if (isMineRevealed) return true;

            if (Fields[row, column].FieldValue == 0)
            {
                List<Tuple<int, int>> surroundingZeroValueFields =
                    new List<Tuple<int, int>> {new Tuple<int, int>(row, column)};

                while (surroundingZeroValueFields.Count != 0)
                {
                    IterateSurrounding(surroundingZeroValueFields[0].Item1, surroundingZeroValueFields[0].Item2, false, surroundingZeroValueFields);

                    surroundingZeroValueFields.RemoveAt(0);
                }

            }

            return false;
        }

        public void MarkField(int row, int column)
        {
            Fields[row, column].Mark();
        }

        private BoardField[,] PopulateBoard(int numberOfRows, int numberOfColumns, int numberOfMines)
        {
            var board = new BoardField[numberOfRows, numberOfColumns];
            var minesLocations = GetMinesLocations(numberOfRows, numberOfColumns, numberOfMines);

            int counter = 0;
            for (int row = 0; row < numberOfRows; row++)
            {
                for (int column = 0; column < numberOfColumns; column++)
                {
                    board[row, column] =
                        minesLocations.Contains(counter) ? new BoardField(row, column, true) : new BoardField(row, column, false);
                    counter++;
                }
            }
            return board;
        }

        private void SetFieldsValues()
        {
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    IterateSurrounding(row, column, true, new List<Tuple<int, int>> { new Tuple<int, int>(row, column) });
                }
            }
        }

        //not a good example of method - added to play a bit with switch case and local methods
        private void IterateSurrounding(int boardRow, int boardColumn, bool setValues,
            List<Tuple<int, int>> fieldsLocationList)
        {
            for (int row = boardRow - 1; row <= boardRow + 1; row++)
            {
                if (row < 0 || row > NumberOfRows - 1) continue;

                for (int column = boardColumn - 1; column <= boardColumn + 1; column++)
                {
                    if (column < 0 || column > NumberOfColumns - 1) continue;

                    //switch case example
                    switch (setValues)
                    {
                        case true:
                            SetFieldValue();
                            break;
                        case false:
                            RevealSurroundingZeros();
                            break;
                        default:
                            SetFieldValue();
                            break;
                    }

                    //local methods example
                    void SetFieldValue()
                    {
                        if (Fields[row, column].HasMine) Fields[boardRow, boardColumn].FieldValue++;
                    }

                    //require fields to have values set
                    void RevealSurroundingZeros()
                    {
                        if (Fields[row, column].FieldValue == 0 && !Fields[row, column].IsRevealed)
                            fieldsLocationList.Add(new Tuple<int, int>(row, column));
                        if (!Fields[row, column].IsRevealed && !Fields[row, column].HasMine) Fields[row, column].Reveal();
                    }
                }
            }
        }

        private int[] GetMinesLocations(int numberOfRows, int numberOfColumns, int numberOfMines)
        {
            int[] minesLocations = new int[numberOfMines];
            Random random = new Random();

            for (int i = 0; i < numberOfMines; i++)
            {
                var randomLocation = random.Next(0, (numberOfRows * numberOfColumns) + 1);
                while (!minesLocations.Contains(randomLocation))
                {
                    minesLocations[i] = randomLocation;
                }
            }
            return minesLocations;
        }
    }
}
