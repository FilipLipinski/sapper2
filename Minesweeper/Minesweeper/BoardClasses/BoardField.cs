﻿namespace Minesweeper.BoardClasses
{
    public class BoardField
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public int FieldValue { get; set; } = 0;
        public bool HasMine { get; set; }
        public bool IsRevealed { get; set; } = false; // C# 6.0 feature - default in-line value
        public bool IsMarked { get; set; } = false;

        public BoardField(int row, int column, bool hasMine)
        {
            Row = row;
            Column = column;
            HasMine = hasMine;
        }

        public bool Reveal()
        {
            IsRevealed = true;

            return HasMine;
        }

        public void Mark()
        {
            IsMarked = true;
        }
    }

}
